#!/usr/bin/python3

import webapp
from urllib.parse import unquote

Error: str = "404 Error"
Success: str = "200 OK"
Redirect: str = "308 Permanent Redirect"

# Formulario por defecto que se enviará a los clientes
form = """
    <h1>Recortadora de URL's</h1>
    <form action=" " method="POST">
    <label for="url">URL A ACORTAR:</label><input type="text" name="url"><br>
    <label for="url">NUEVA URL:</label><input type="text" name="short"><br>
    <input type="submit" value="ENVIAR">
    </form>
    """


class Shortener(webapp.webApp):
    # Inicializamos nuestras variable url_list, que contiene el diccionario de urls del programa
    url_list: dict = {}

    # Nos devuelve el método,respuesta y resto de la solicitud
    def parse(self, request):

        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        rest = unquote(request.split('\r\n\r\n', 1)[1])

        if method is None:
            return None
        else:
            return method, resource, rest

    # Fase de procesado de la petición en función del método(GET o POST y el contenido resource)
    def process(self, parsedrequest):

        httpcode: str
        htmlbody: str
        (method, resource, rest) = parsedrequest

        # El metodo que llega no nos interesa y desechamos la operación
        if (method != "GET") and (method != "POST"):
            httpcode = Error
            htmlbody = "<html><body>" + "El recurso solicitado no está disponible" "</body></html>"

        # Valor de metodo GET
        elif method == "GET":
            # El cliente envía una solicitud ""vacía"" (solo '/')
            if resource == "/":
                # El servidor envía un estado = 200 OK, el formulario y la lista de urls que sabe
                httpcode = Success
                htmlbody = "<html><body>" + form + "Lista de urls acortadas disponibles: " \
                           + str(self.url_list) + "</body></html>"
            elif resource in self.url_list:
                # El servidor envía el estado de redireccionado y a continuación redirecciona a la página solicitada
                httpcode = Redirect
                htmlbody = "<html><body> Redireccionando a " + str(self.url_list[resource]) + "..." \
                           + '<meta http-equiv="refresh" content="2;url=' + self.url_list[resource] + '">' \
                           + "</body></html>"
            else:
                # No se ha encontrado el recurso solicitado
                httpcode = Error
                htmlbody = "<html><body> El recurso solicitado no se encuentra disponible.</body></html>"

        # Valor de metodo POST (trabajar con variables del formulario)
        else:
            # Almacenamiento de la url a acortar
            url = unquote(rest.split("&")[0].split("=")[1])

            # Comprobar si se ha rellenado con las especificaciones o la añadimos nosotros
            if (not url.find('http://')) or (not url.startswith('https://')):
                url = "http://" + url

            # Almacenamiento de nuestra nueva url acortada
            short = "/" + rest.split("&")[1].split("=")[1]

            # Ver si el nombre escogido ya está en uso
            if short in self.url_list:
                httpcode = Redirect
                htmlbody = "<html><body>" + "<h1>ERROR: </h1>El nombre elegido para acortar ya esta en uso, \
                               por favor, seleccione un nombre nuevo"\
                            + "<p>OJO: Volviendo de nuevo a http://localhost:1234/ </p>"\
                            + '<meta http-equiv="refresh" content="7;url=' + "http://localhost:1234/" + '">' \
                            + "</body></html>"
            else:
                # Incluir la nueva url en el diccionario
                self.url_list[short] = url

                """
                Enviamos al cliente un código 200 OK y a continuación mostramos las modificaciones al cliente. 
                Redireccionamos a la página inicial
                
                """
                httpcode = Redirect
                htmlbody = "<html><body><h1>URL acortada satisfactoriamente</h1>" \
                           + "URL SIN ACORTAR:" '<a href="' + self.url_list[short] + ' ">"' + self.url_list[
                               short] + '"</a><br>' \
                           + "URL ACORTADA:  " '<a href="' + self.url_list[short] + ' ">"' + short + '"</a><br>' \
                           + "<p>OJO: Vas a ser redireccionado a la pagina anterior, " \
                             "para ver la URL pulsa en ella.</p>" \
                           + '<meta http-equiv="refresh" content="7;url=' + "http://localhost:1234/" + '">' \
                           + "</body></html>"

        return httpcode, htmlbody


if __name__ == "__main__":
    testWebApp = Shortener("localhost", 1234)
